import React from "react";
import {
  BrowserRouter as Router,
  Link
} from "react-router-dom";

import Timeline from './timeline.js'


class Education extends React.Component {

    constructor() {
        super();
        this.state = {
          curtainStyle: "curtain ",
          nameStyle: "name "
        }
        this.menuAnimation = this.menuAnimation.bind(this)
    }



    componentDidMount(){
    }

    componentWillUnmount() {
    }

    menuAnimation () {
      this.setState({curtainStyle: "curtain animate ", nameStyle:"name animate "})
      
    }

    render() {


        return (
          <div>
              <div className="nav">
                <Link to="/education" className="sectionTitle">EXPERIENCE</Link>
                <Link to="/projects" className="otherTitles">PROJECTS</Link>
                <Link to="/about" className="otherTitles">ABOUT</Link>

              </div>


                <Timeline />
            
            <div className="footer">
                <p>Rui Calheno</p>
            </div>
          </div>
        )
    }
}

export default Education
