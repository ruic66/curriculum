import React from "react";
import {
  BrowserRouter as Router,
  Link
} from "react-router-dom";


class Home extends React.Component {

    constructor() {
        super();
        this.state = {
          curtainStyle: "curtain ",
          nameStyle: "name ",
          navStyle:"navList "
        }
        this.menuAnimation = this.menuAnimation.bind(this)
    }



    componentDidMount(){
    }

    componentWillUnmount() {
    }

    menuAnimation () {
      this.setState({curtainStyle: "curtain animate ", nameStyle:"name animate ", navStyle:"navList animate "})
      
    }

    render() {


        return (
          <div>
            <div className={this.state.curtainStyle} />
            <h1 className={this.state.nameStyle}>
            RUI CALHENO
          </h1>
          <div className={this.state.navStyle}>
            <Link to="/projects" className="item" >PROJECTS</Link>
            <p className="item separator">&nbsp;</p>
            <Link to="/about" className="item">ABOUT</Link>
          </div>
          </div>
        )
    }
}

export default Home
