import React from "react";
import {
  BrowserRouter as Router,
  Link
} from "react-router-dom";


class About extends React.Component {

    constructor() {
        super();
        this.state = {
          curtainStyle: "curtain ",
          nameStyle: "name "
        }
        this.menuAnimation = this.menuAnimation.bind(this)
    }



    componentDidMount(){
    }

    componentWillUnmount() {
    }

    menuAnimation () {
      this.setState({curtainStyle: "curtain animate ", nameStyle:"name animate "})
      
    }

    render() {


        return (
          <div>
              <div className="nav">
                <Link to="/about" className="sectionTitle">ABOUT</Link>
                <Link to="/projects" className="otherTitles">PROJECTS</Link>
                <Link to="/education" className="otherTitles">EXPERIENCE</Link>

              </div>
                <div className="about">
                    <div className="img-wrap">
                    <img className="me " src="/me.jpg" />
                    </div>
                    <div className="info">
                    <p >Computer Science MSc</p>
                    <p >22 years of age</p>
                    <p >Web Developer</p>
                    <p >Photographer</p>
                    </div>

                </div>
            <div className="footer">
                <p>Rui Calheno</p>
            </div>
          </div>
        )
    }
}

export default About
