import React from "react";
import {
  BrowserRouter as Router,
  Link
} from "react-router-dom";
import AOS from 'aos'
import { FaWrench, FaQuoteLeft, FaGithub } from "react-icons/fa";

class Project extends React.Component {

    constructor() {
        super();
    }



    componentDidMount(){
    }

    componentWillUnmount() {
    }

    render() {

        AOS.init()

        const tools = this.props.project.tools.map((tool, index) => {
            if (index != this.props.project.tools.length - 1)
                return <p className="tools">{tool} &nbsp; / </p>
            else 
                return <p className="tools">{tool}</p>
        })

        const thumbnails = this.props.project.thumbnails.map(src => {
            return <img className="image" src={src} />
        })


        return (
            <div className="project" data-aos="fade-right">
                <p>
                    {this.props.project.title}
                </p>

                {
                    this.props.project.description
                    &&
                    <p className="description">
                    <FaQuoteLeft /> &nbsp; &nbsp;{this.props.project.description}
                    </p>
                }

                <p className="madeUsing">
                    <FaWrench /> &nbsp; &nbsp;{tools}
                </p>

                <p className="madeUsing">
                    {thumbnails}
                </p>

                <p className="goTo">
                    <a target="blank">Github</a>
                    <a href={this.props.project.projectSource} target="blank">See project</a>
                </p>
                </div>
        )
    }
}

export default Project
