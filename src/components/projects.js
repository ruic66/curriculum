import React from "react";
import {
  BrowserRouter as Router,
  Link
} from "react-router-dom";
import Project from "./project";


const webfolio = {
    title: "Personal Photography Website",
    tools: ["Next.js", "React.js"],
    thumbnails: ["webfolio.png"],
    projectSource: "https://mywebfolio.herokuapp.com"
}

const okapi = {
    title: "Okapi Intelligent Fashion Adviser",
    tools: ["Adonis.js", "MySQL", "MongoDB", "Python", "Vue.js"],
    thumbnails: ["okapi.png"],
    projectSource: ""
}



class Home extends React.Component {

    constructor() {
        super();
        this.state = {
          curtainStyle: "curtain ",
          nameStyle: "name "
        }
        this.menuAnimation = this.menuAnimation.bind(this)
    }



    componentDidMount(){
    }

    componentWillUnmount() {
    }

    menuAnimation () {
      this.setState({curtainStyle: "curtain animate ", nameStyle:"name animate "})
      
    }

    render() {


        return (
          <div>
              <div className="nav">
                <Link to="/projects" className="sectionTitle">PROJECTS</Link>
                <Link to="/education" className="otherTitles">EXPERIENCE</Link>
                <Link to="/about" className="otherTitles">ABOUT</Link>

              </div>


            <div className="projectList">
                <Project project={webfolio} />
                <Project project={okapi} />
            </div>
            <div className="footer">
                <p>Rui Calheno</p>
            </div>
          </div>
        )
    }
}

export default Home
