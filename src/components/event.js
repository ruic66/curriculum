import React from "react";
import {
  BrowserRouter as Router,
  Link
} from "react-router-dom";
import AOS from 'aos'
import { FaMapMarkerAlt, FaGraduationCap, FaLightbulb, FaHandsHelping, FaQuoteLeft, FaDumbbell } from "react-icons/fa";

class Event extends React.Component {

    constructor() {
        super();
    }



    componentDidMount(){
    }

    componentWillUnmount() {
    }

    render() {

        AOS.init()

        let eventMarker = null

        if (this.props.event.type == "Education") {
            eventMarker = <FaGraduationCap/>
        } else if (this.props.event.type == "Research"){
            eventMarker = <FaLightbulb/>
        } else if (this.props.event.type == "Volunteer"){
            eventMarker = <FaHandsHelping />
        }

        const skills = this.props.event.skills.map((skill, index) => {
            if (index != this.props.event.skills.length - 1)
            return <span className="tools">{skill} &nbsp; / </span>
        else 
        return <span className="tools">{skill}</span>
        })

        return (
                <div className="tl-content-event" >
                    <h2 className="tl-event-date" data-aos="fade-right">
                        {this.props.event.startDate} 
                        <br/> 
                        {this.props.event.endDate} 
                    </h2>

                    <h2 className="tl-event-marker" data-aos="fade-in">
                        {eventMarker}
                    </h2>
                      <h2 className="tl-event-title" data-aos="fade-left">
                          {this.props.event.title}
                      </h2>
                      <p className="tl-event-description" data-aos="fade-left">
                         <FaMapMarkerAlt /> &nbsp; {this.props.event.location}
                      </p>
                      {
                          this.props.event.description 
                          && 
                          <p className="tl-event-description" data-aos="fade-left">
                          <FaQuoteLeft /> &nbsp; {this.props.event.description}
                          </p>
                      }

                      <p className="tl-event-description" data-aos="fade-left">
                      <FaDumbbell /> &nbsp; {skills}
                      </p>
                  </div>
        )
    }
}

export default Event
