import React from "react";
import AOS from 'aos'
import window from 'global'
import Event from './event.js'
import { FaMapMarkerAlt, FaGraduationCap, FaLightbulb } from "react-icons/fa";

const university = {
    startDate: "Sept 2015",
    endDate: "Present",
    type: "Education",
    title: "MSc in Computer Science",
    skills: ['Docker', 'C', 'Arduino'],
    location: "University of Minho, Braga, Portugal"
}

const greenlab = {
    startDate: "April 2019",
    endDate: "Dec 2019",
    type: "Research",
    title: "Research",
    description: "Comparision of different Database Management Systems, regarding its energy consumption during the execution of TPC-H and TPC-C benchmarks.",
    skills: ['Docker', 'C', 'Arduino'],
    location: "Greenlab, Braga, Portugal"
}

const fm19 = {
    startDate: "Oct 2019",
    endDate: "",
    type: "Volunteer",
    skills: ['Team work', 'Problem-solving'],
    title: "Formal Methods 19 Conference volunteer",
    location: "Porto, Portugal"
}

class Timeline extends React.Component {

    constructor() {
        super();
        this.state = {
            time: 0
        }
    }

    componentDidMount(){
        window.addEventListener('scroll', ()=> {
            this.setState({time: window.pageYOffset})
            console.log(window.pageYOffset)
        })
    }

    render() {
        AOS.init();

        return (
          <div className="tl">
              <div className="tl-line" style={{height:  'calc(50vh + ' + this.state.time + 'px)' }}/>
              <div className="tl-content">
                    <Event event={university} />
                    <Event event={greenlab} />
                    <Event event={fm19} />
              </div>
          </div>
        )
    }
}

export default Timeline
