import React from "react";
import {
  BrowserRouter as Router,
  Link,
  withRouter,
  Switch,
  Route
} from "react-router-dom";
import { TransitionGroup, CSSTransition } from 'react-transition-group'
import Home from './home.js'
import About from './about.js'
import Projects from './projects.js'
import Education from "./education.js";

function MainPage({location}) {
    return (
<TransitionGroup component={null}>
            <CSSTransition
              key={location.key}
              timeout={{enter: 2200, exit: 900}}
              classNames='page-transition'
            >


      <Switch location={location}>
          <Route path="/about">
              <About />
          </Route>
          <Route path="/projects">
            <Projects />
          </Route>
          <Route path="/work">
          </Route>
          <Route path="/education">
              <Education />
          </Route>
          <Route path="/">
            <Home />
          </Route>
        </Switch>

        </CSSTransition>
            </TransitionGroup>
    )
}


export default withRouter(MainPage)
