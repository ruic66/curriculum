import React from 'react';
import logo from './logo.svg';
import './App.css';

import MainPage from './components/mainpage.js'

import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";

function App() {
  return (
    <Router>
      <div className="App">
        <MainPage />
      </div>
    </Router>

  );
}

export default App;
